ladvd (1.1.2-4) unstable; urgency=medium

  * QA upload.

  * Added d/gbp.conf to describe branch layout.
  * Updated vcs in d/control to Salsa.
  * Updated d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 4.6.1 to 4.7.0.
  * Use wrap-and-sort -at for debian control files
  * Replaced obsolete pkg-config build dependency with pkgconf.
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Set field Upstream-Name in debian/copyright.
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

 -- Petter Reinholdtsen <pere@debian.org>  Tue, 04 Jun 2024 15:37:52 +0200

ladvd (1.1.2-3) unstable; urgency=medium

  * QA upload.
  * Remove outdated Vcs control fields.

  [ Helmut Grohne ]
  * Defer location of systemd unit to systemd.pc. (Closes: #1054014)

 -- Chris Hofstaedtler <zeha@debian.org>  Sat, 25 Nov 2023 14:13:46 +0100

ladvd (1.1.2-2) unstable; urgency=medium

  * QA upload.
  * Set Debian QA as maintainer. See #986334
  * Fix ftbfs. (Closes: #994677)
  * Protect against future vis prototype variant switch,
    (Closes: #979662)
  * Add upstream/metadata
  * d/control:
    - Bump debhelper to 13.
    - Update Standards-Version to 4.6.1
  * d/rules:
    -  use dh_installsystemd instead of dh_systemd_enable

 -- Bo YU <tsu.yubo@gmail.com>  Sun, 23 Oct 2022 14:10:08 +0800

ladvd (1.1.2-1) unstable; urgency=medium

  * New upstream release. Fixes:
    - a segfault on startup on some systems. (Closes: #890051)
  * Depend on pciutils. (Closes: #900897)

 -- Marco d'Itri <md@linux.it>  Mon, 16 Jul 2018 00:26:03 +0200

ladvd (1.1.1~pre1-2) unstable; urgency=medium

  * Add patch no_werror: do not use -Werror. (Closes: #808499)

 -- Marco d'Itri <md@linux.it>  Sun, 27 Dec 2015 14:41:24 +0100

ladvd (1.1.1~pre1-1) unstable; urgency=medium

  * New upstream snapshot. Fixes:
    + AC_CC_STACK_PROTECTOR check on hppa. (Closes: #780891)
  * Added a Homepage field to debian/control. (Closes: #787475)

 -- Marco d'Itri <md@linux.it>  Sat, 01 Aug 2015 15:53:26 +0200

ladvd (1.1.0-1) unstable; urgency=medium

  * New upstream release.
  * New maintainer, not anymore a native package.

 -- Marco d'Itri <md@linux.it>  Fri, 20 Mar 2015 15:23:54 +0100

ladvd (1.0.4) unstable; urgency=low

  * New upstream release

 -- Sten Spans <sten@blinkenlights.nl>  Sat, 18 Feb 2012 16:37:56 +0100

ladvd (1.0.3) unstable; urgency=low

  * New upstream release

 -- Sten Spans <sten@blinkenlights.nl>  Sun, 12 Feb 2012 22:11:16 +0100

ladvd (1.0.2) unstable; urgency=low

  * New upstream release

 -- Sten Spans <sten@blinkenlights.nl>  Tue, 07 Feb 2012 08:33:09 +0100

ladvd (1.0.1) unstable; urgency=low

  * New upstream release

 -- Sten Spans <sten@blinkenlights.nl>  Mon, 30 Jan 2012 15:22:28 +0100

ladvd (0.9.2-2) unstable; urgency=low

  * Disable tests during build. Closes: #650670
  * Add gcc 4.6 fixes. Closes: #625363

 -- Sten Spans <sten@blinkenlights.nl>  Thu, 22 Dec 2011 12:07:36 +0200

ladvd (0.9.2-1.1) unstable; urgency=low

  * Non maintainer upload.
  * Build with -Wno-error=unused-but-set-variable. Closes: #625363.

 -- Matthias Klose <doko@debian.org>  Sun, 04 Sep 2011 02:17:51 +0200

ladvd (0.9.2-1) unstable; urgency=low

  * Initial release (Closes: #572336)

 -- Sten Spans <sten@blinkenlights.nl>  Mon, 27 Sep 2010 10:44:36 +0200
